﻿using System;
using System.Drawing; // Image class
using System.Media; // SoundPlayer class

namespace WindowSitter
{
    public class CWindowSitter
    {
        private Image MainImage = null; // Main image of the sitter graphic
        private Image AltImage = null; // Alternative image of the sitter which will appear randomly
        private bool IsBlinking = false; // Is currently blinking?
        private int BlinkInterval = 5; // Time to wait in between blinks (seconds)
        private int BlinkLength = 200; // How long to hold the blink (miliseconds)
        private DateTime LastBlink = new DateTime(); // Last time since blink was started
        private SoundPlayer AlertSound = null; // Sound which will be played upon clicking the sitter
        Color ChromaKey = new Color(); // Color which will be chromakeyed by the window
        Random Rand;
        private bool Hidden;

        public int OffsetX = 180; // Sitter X Position = ActiveWindow.Right - OffsetX
        public int OffsetY = 85; // Sitter Y Position = ActiveWindow.Top - OffsetY
        public readonly string[] IgnoredWindows = { "Windows Task Manager" };

        private Image LoadImage(string FileName)
        {
            Bitmap FileBitmap = new Bitmap(FileName);
            FileBitmap.MakeTransparent(ChromaKey);
            return FileBitmap;
        }

        private void BeginBlink()
        {
            IsBlinking = true;
            LastBlink = DateTime.UtcNow;
        }

        private void EndBlink()
        {
            IsBlinking = false;
            BlinkInterval = Rand.Next(5, 45); // Randomize blink frequency for better effect
        }

        public void LoadAltImage(string FileName)
        {
            AltImage = LoadImage(FileName);
        }

        public void LoadSound(string FileName)
        {
            AlertSound = new SoundPlayer(FileName);
        }

        public CWindowSitter(string MainImageFileName, Color ChromaKeyColor)
        {
            Rand = new Random();
            ChromaKey = ChromaKeyColor;
            MainImage = LoadImage(MainImageFileName);
            Hidden = true;
            LastBlink = DateTime.UtcNow;
        }

        public Size GetImageSize()
        {
            return new Size(MainImage.Width, MainImage.Height);
        }

        public Image GetImage()
        {
            if (AltImage == null)
                return MainImage;

            TimeSpan TimeSinceLastBlink = DateTime.UtcNow - LastBlink;
            TimeSpan TimeSinceStartedBlink = DateTime.UtcNow - LastBlink;

            if (!IsBlinking && TimeSinceLastBlink.TotalSeconds > BlinkInterval)
            {
                // Blink has started
                BeginBlink();
            }
            else
            {
                // Currently mid-blink
                if (TimeSinceStartedBlink.TotalMilliseconds > BlinkLength)
                {
                    // Blink length reached
                    EndBlink();
                }
            }

            if (IsBlinking)
                return AltImage;
            else
                return MainImage;
        }

        public void ForceBlink() { if (AltImage != null) BeginBlink(); }
        public void PlaySound() { if (AlertSound != null) AlertSound.Play(); }
        public bool IsHidden() { return Hidden; }
        public void Hide() { Hidden = true; }
        public void Show() { Hidden = false; }
    } // End of WindowSitter class definition
}

﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;
using System.Drawing.Drawing2D;
using System.IO;
using System.Configuration;
using WindowSitter;

namespace Sitter
{
    public partial class HiddenWindow : Form
    {
        /* ==================
        *      Imports
        * ==================*/
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
        [DllImport("user32.dll")]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;        // x position of upper-left corner  
            public int Top;         // y position of upper-left corner  
            public int Right;       // x position of lower-right corner  
            public int Bottom;      // y position of lower-right corner  
        }

        /* =====================
        *      Variables
        * =====================*/
        private CWindowSitter Sitter;
        static readonly Color TRANSPARENCY_COLOR = Color.LimeGreen;
        private bool Initialized = false;

        /* ==================
        *      Methods
        * ==================*/
        private string GetWindowName(IntPtr hWindow)
        {
            const int MAX_CHARS = 256;
            StringBuilder Buffer = new StringBuilder(MAX_CHARS);

            if (GetWindowText(hWindow, Buffer, MAX_CHARS) > 0)
                return Buffer.ToString();
            return null;
        }

        private bool IsSitterVisible(RECT WindowRect)
        {
            // Left and Top will be -8 if the window is fullscreen
            if (WindowRect.Left == -8 || WindowRect.Top <= 0)
                return false;
            else
                return true;
        }

        private void CheckFile(string FilePath)
        {
            if (!File.Exists(FilePath))
                CriticalError("File not found", "File '" + FilePath + "' does not exist!");
        }

        void CriticalError(string Caption, string Text)
        {
            MessageBox.Show(this, Text, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Environment.Exit(1);
        }
        
        /* ==================
         *      Events
         * ==================*/

        public HiddenWindow()
        {
            InitializeComponent();

            this.BackColor = TRANSPARENCY_COLOR;
            this.TransparencyKey = TRANSPARENCY_COLOR;
            this.FormBorderStyle = FormBorderStyle.None;

            // Load values from the configuration file
            string ImageSrc = ConfigurationManager.AppSettings["ImageSrc"];
            string AltImageSrc = ConfigurationManager.AppSettings["AltImageSrc"];
            string SoundSrc = ConfigurationManager.AppSettings["SoundSrc"];

            int OffsetX = 0;
            int OffsetY = 0;
            string szOffsetX = ConfigurationManager.AppSettings["OffsetX"];
            string szOffsetY = ConfigurationManager.AppSettings["OffsetY"];

            try
            {
                OffsetX = Convert.ToInt32(szOffsetX);
                OffsetY = Convert.ToInt32(szOffsetY);
            }
            catch (FormatException ex)
            {
                CriticalError("Configuration Error (Format exception)", "Failed to load offset values from the configuration file!\nValue must be a signed 32-bit integer!");
            }
            catch (OverflowException ex)
            {
                CriticalError("Configuration Error (Overflow exception)", "Failed to load offset values from the configuration file!\nValue must be a signed 32-bit integer!");
            }

            if (ImageSrc != null && ImageSrc != "")
            {
                CheckFile(ImageSrc);
                Sitter = new CWindowSitter(ImageSrc, TRANSPARENCY_COLOR);
            }
            else
                CriticalError("Configuration Error", "Failed to load value 'ImageSrc' from the configuration file!\nMake sure the file exists in the specified directory");

            if (AltImageSrc != null && AltImageSrc != "")
            {
                CheckFile(AltImageSrc);
                Sitter.LoadAltImage(AltImageSrc);
            }

            if (SoundSrc != null && SoundSrc != "")
            {
                CheckFile(SoundSrc);
                Sitter.LoadSound(SoundSrc);
            }

            Sitter.OffsetX = OffsetX;
            Sitter.OffsetY = OffsetY;

            var ImageSize = Sitter.GetImageSize();
            this.Width = ImageSize.Width;
            this.Height = ImageSize.Height;

            UpdateTimer.Enabled = true;
            RedrawTimer.Enabled = true;
            Initialized = true;
        }

        private void HiddenWindow_Paint(object sender, PaintEventArgs e)
        {
            if (Initialized)
            {
                Graphics GDI = this.CreateGraphics();
                GDI.SmoothingMode = SmoothingMode.AntiAlias;
                GDI.InterpolationMode = InterpolationMode.Bicubic;
                GDI.PixelOffsetMode = PixelOffsetMode.HighQuality;
                if (!Sitter.IsHidden())
                    GDI.DrawImage(Sitter.GetImage(), new Point(0, 0));
            }
        }

        private void HiddenWindow_Load(object sender, EventArgs e)
        {
        }

        // Updates the sitter position, snapping it to the active window
        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            var hWindow = GetForegroundWindow();
            if (hWindow != IntPtr.Zero)
            {
                GetWindowRect(hWindow, out RECT WindowRect);
                string WindowName = GetWindowName(hWindow);

                // Hide the sitter graphic if we cannot get the active window name
                // or if the active window is in the blacklist
                if (WindowName == null || Array.IndexOf(Sitter.IgnoredWindows, WindowName) >= 0)
                {
                    Sitter.Hide();
                    return;
                }

                // Do not adjust the position if the active window is itself, but do continue drawing it if already being drawn
                // Can happen if the sitter is positioned over a window and gets clicked
                if (WindowName != "WindowSitter")
                {
                    if (IsSitterVisible(WindowRect))
                    {
                        int OffsetX, OffsetY;
                        OffsetX = Sitter.OffsetX;
                        OffsetY = Sitter.OffsetY;

                        Size WindowSize = new Size(WindowRect.Right - WindowRect.Left, WindowRect.Bottom - WindowRect.Top);

                        // In case the window is not wide enough, move the sitter a bit closer to the edge
                        if (WindowSize.Width < OffsetX)
                            OffsetX = 70;

                        this.Location = new Point(WindowRect.Right - OffsetX, WindowRect.Top - OffsetY);
                        Sitter.Show();
                    }
                    else
                        Sitter.Hide();
                }
            }
        }

        private void HiddenWindow_MouseUp(object sender, MouseEventArgs e)
        {
            Rectangle HitBox = new Rectangle(new Point(0, 0), Sitter.GetImageSize());
            if (HitBox.Contains(e.X, e.Y))
            {
                if (e.Button == MouseButtons.Right)
                {
                    Point Origin = new Point();
                    Origin.X = this.Location.X + e.X;
                    Origin.Y = this.Location.Y + e.Y;
                    ContextMenu.Show(Origin);
                }
                if (e.Button == MouseButtons.Left)
                {
                    Sitter.PlaySound();
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var About = new AboutWindow();
            About.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void RedrawTimer_Tick(object sender, EventArgs e)
        {
            // Force a redraw every X miliseconds
            this.Invalidate();
        }

        private void HiddenWindow_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle HitBox = new Rectangle(new Point(0, 0), Sitter.GetImageSize());
            if (HitBox.Contains(e.X, e.Y))
                Sitter.ForceBlink();
        }
    }
}

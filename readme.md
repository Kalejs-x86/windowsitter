# WindowSitter for Windows

WindowSitter is a small mascot that follows you when you're working on your computer, appearing on top of the currently active window, it is a digital companion that adds a twist of fun to your everyday tasks.

This software encapsulates the idea behind the MaCoPiX project, simplifies it and brings it to the Microsoft Windows environment.

## Setting up

### Requirements

* A machine running Windows 7 or higher
* .NET Framework version 4.0 or higher

### Installation

Download and run the newest release [here](https://bitbucket.org/Kalejs-x86/windowsitter/downloads/WindowSitter%201.0.7z).
The default package comes with an example mascot and an alert sound.

## Customization

When WindowSitter is run, it reads the values in the WindowSitter.exe.config file to determine where the resources it needs are located and where the mascot graphic will be displayed on the window.

The default config should look like this

```
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <startup> 
        <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.6.1"/>
    </startup>
  <appSettings>
    <add key="ImageSrc" value="res\WSMain.png"/>
    <add key="AltImageSrc" value="res\WSAlt.png"/>
    <add key="SoundSrc" value="res\Alert.wav"/>
    <add key="OffsetX" value="180"/>
    <add key="OffsetY" value="85"/>
  </appSettings>
</configuration>
```

This is what the variables describe

**ImageSrc** Relative or full path to the main mascot graphic

**AltImageSrc** Relative or full path to the alternative mascot graphic

**SoundSrc** Relative or full path to the alert sound

**OffsetX** and **OffsetY** are used to adjust the position of the mascot on the active window, moving it in the specified direction by the specified amount of pixels

## Credits

* [Piper21 on AminoApps](https://aminoapps.com/c/undertale/page/user/piper21/RXru_JfkKRz3KenPRGN7awx5Jem5j) for the 'Cute Toriel' drawing, which I've scaled down and added transparency to for use as an example mascot
* Toby Fox for the intellectual property and the 'alert.wav' example sound effect which is taken from the game 'Undertale'

## License

This project is licensed under the MIT License - see the [LICENSE.md](../LICENSE.md) file for details